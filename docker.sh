#!/bin/bash

docker_start()
{
	docker run --name server1 -d -P chusiang/ansible-managed-node:ubuntu-16.04
	docker run --name server2 -d -P chusiang/ansible-managed-node:ubuntu-18.04
	server1_orig_port=$(cat hosts| grep ansible_ssh_host | grep server1 | cut -d'=' -f3| cut -d' ' -f1)
	server2_orig_port=$(cat hosts| grep ansible_ssh_host | grep server2 | cut -d'=' -f3| cut -d' ' -f1)
	server1_new_port=$(docker ps | grep server1| cut -d':' -f3| cut -d'-' -f1)
	server2_new_port=$(docker ps | grep server2| cut -d':' -f3| cut -d'-' -f1)
	sed -i "s/$server1_orig_port/$server1_new_port/g" hosts 
	sed -i "s/$server2_orig_port/$server2_new_port/g" hosts
	echo "server1 ssh port: $server1_new_port"
	echo "server2 ssh port: $server2_new_port"
}

docker_stop()
{
	docker rm -f server1 server2
}

run_playbook()
{
	ansible-playbook $1
}

docker_ssh()
{
	local server=$1
	local port=$(docker ps | grep $server| cut -d':' -f3| cut -d'-' -f1)
	ssh localhost -p $port -l docker
}

case $1 in
	"start")
		docker_start
		;;
	"stop")
		docker_stop
		;;
	"ansible")
		run_playbook $2
		;;
	"ssh")
		docker_ssh $2
		;;
	*)
		echo "./docker.sh [start|stop|ssh|ansible]"
esac
