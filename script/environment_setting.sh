#!/bin/bash
# File              : environment_setting.sh
# Author            : Keats Hu <agg2567@mail.com>
# Date              : 2018-08-18
# Last Modified Date: 2018-08-18
# Last Modified By  : Keats Hu <agg2567@mail.com>

# VIM Refereance: https://wizardforcel.gitbooks.io/use-vim-as-ide/content/2.html

vim_install()
{
	# chech VIM been installed
	# Download vundle plugin (vim plugin management tool)
	git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
	# Download Theme dante (https://github.com/vim-scripts/dante.vim) and add config 
	git clone https://github.com/vim-scripts/dante.vim
	mkdir -p ~/.vim/colors
	cp dante.vim/colors/dante.vim ~/.vim/colors/
	echo 'hi CursorColumn ctermbg=236 cterm=bold guifg=white guibg=yellow gui=bold' >> ~/.vim/colors/dante.vim
	echo 'hi CursorLine ctermbg=236 cterm=bold guifg=white guibg=yellow gui=bold' >> ~/.vim/colors/dante.vim
	rm -rf dante.vim
	#	`hi CursorColumn ctermbg=236 cterm=bold guifg=white guibg=yellow gui=bold`
	#	`hi CursorLine ctermbg=236 cterm=bold guifg=white guibg=yellow gui=bold`
	# Download Keats' VIM Configuration
	wget http://www.cs.nctu.edu.tw/~shhu/configuration/vimrc -O ~/.vimrc	
	# Install Plugins `vim -c PluginInstall -c quitall`
	vim -c PluginInstall -c quitall
	vim -c GoInstallBinaries -c quitall
}

vim_install
